<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sample' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K``aAc;Qr~@b)zi<Yg%@@@K:e6eJ8B/Qk>N1~G`Lg^%=(wu}GV89: oSbnF7|TDk' );
define( 'SECURE_AUTH_KEY',  '#qTP6XeF[geij{LCAK)IB5@z3Q!KD{33ED:ZOO 4[1|->@f[Uf$FYrEW<@C,zG![' );
define( 'LOGGED_IN_KEY',    '*[4d5q1[zhIG@fv%FKlh&9 7:;/eI~NoxPUr$y*lIvm4_Aj+ppkp1#!~Ad0*rhc`' );
define( 'NONCE_KEY',        '{65/7:`7=LwjjY<y *,0`yD_zAlizz5^p_UWa-VL|u5d-@4_syWRl_g5t[F8FH?1' );
define( 'AUTH_SALT',        '+n.ZeA=!7EF}+g;=gx-YrbQR^vapWvU2O.1n;wPs-$W|7u8T~)ew.IYoweTZ8S:g' );
define( 'SECURE_AUTH_SALT', 'ea_6jA3Dq]VaW11`Xd V43$P~| $Sg<]GN<K$p!*-nI,5}U3WJKUm#;nv?U9#P?9' );
define( 'LOGGED_IN_SALT',   'pe~U@R=?Myj/20i.,?]lGRP!Cfq8NE3zI+$>belop-r!#T(H0?Z R!%?nYt_i}6.' );
define( 'NONCE_SALT',       'jF!#@pzzRNi!a-h/6PN&E2s+< AOatMdE},`Pyb7C|Zu%&IRcPSLrG5?G,zD[(9^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
