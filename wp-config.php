<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sample' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5X j#LGt?Ks%B49CSaV/<V|K1=Zbiw5Ov pU9ZXde,U)7m S?+u-+r:e=V?+lBlv' );
define( 'SECURE_AUTH_KEY',  'LwB;uK)N{C=XSpM) 8PCn;By8HrBtDz)<R$6R.YY G/-4oI<-oTBw=Y=QjmiuJc ' );
define( 'LOGGED_IN_KEY',    '7ON:x|3Gu [t^rt7@3$Mq)/i!39%+CjH=8*HckLRKA>Q+4nVcsD8T1CP{a%rh&!$' );
define( 'NONCE_KEY',        'tEM,{A?We] 7c~?VFDne9q?]mE]XPk/JMmKOuY(0xu#W>S#DH.AbDo^Pl;DC&rKJ' );
define( 'AUTH_SALT',        ')Kf;Cz]c6A`Xn-L=9wt=<Z(M{RIr ;;:Au<d}/Grz)#2hG]W<z54nDfyEkkfWkw(' );
define( 'SECURE_AUTH_SALT', '(T&v?962K);>cCu RgI{b{Z:,5q:CedM? wE%LKo;~U{2dYJA*T:sP]v)lw^AQ~b' );
define( 'LOGGED_IN_SALT',   'SzUrTe]@{hmgTfE;EKCI-% o16U&SNR=erW{+_WvT6Vk!V->nC3!L-{g:x*N3zjp' );
define( 'NONCE_SALT',       '|:ccd`]7O+Mt>e/8Yx]Z)E)$~p/8^;1d]BS9h::=NjVE@S8Y*UAI!]& 5*M`ZWvZ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
